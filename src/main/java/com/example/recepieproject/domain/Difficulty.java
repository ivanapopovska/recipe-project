package com.example.recepieproject.domain;

public enum Difficulty {
    EASY, MODERATE, HARD
}
