package com.example.recepieproject.controllers;

import ch.qos.logback.classic.net.SyslogAppender;
import com.example.recepieproject.domain.Category;
import com.example.recepieproject.repositories.CategoryRepo;
import com.example.recepieproject.services.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
public class IndexController {

    private final RecipeService recipeService;

    public IndexController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {

        model.addAttribute("recipes", recipeService.getRecipes());

        return "index";
    }
}
