package com.example.recepieproject.services;

import com.example.recepieproject.commands.RecipeCommand;
import com.example.recepieproject.domain.Recipe;

import java.util.Set;

public interface RecipeService {
    Set<Recipe> getRecipes();

    Recipe findById(Long id);

    RecipeCommand saveRecipeCommand(RecipeCommand command);

    RecipeCommand findCommandById(Long valueOf);

    void deleteById(Long id);
}
