package com.example.recepieproject.services;

import com.example.recepieproject.commands.IngredientCommand;

public interface IngredientService {
    IngredientCommand findByRecipeIdAndIngredientId(Long recipeId, Long ingredientId);

    IngredientCommand findById(Long valueOf);

    IngredientCommand saveIngredientCommand(IngredientCommand command);

    void removeIngredient(Long valueOf, Long valueOf1);

    void delete(Long valueOf, Long valueOf1);
}
