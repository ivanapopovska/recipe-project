package com.example.recepieproject.services;

import com.example.recepieproject.commands.UnitOfMeasureCommand;
import com.example.recepieproject.domain.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface UnitOfMeasureService  {
    Set<UnitOfMeasureCommand> listAllUoms();

}
