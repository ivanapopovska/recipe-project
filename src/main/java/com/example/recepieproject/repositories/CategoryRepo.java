package com.example.recepieproject.repositories;

import com.example.recepieproject.domain.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


public interface CategoryRepo extends CrudRepository<Category, Long> {

    Optional<Category> findByDescription(String description);
}
