package com.example.recepieproject.repositories;

import com.example.recepieproject.domain.Recipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface RecipeRepo extends CrudRepository<Recipe, Long> {
}
