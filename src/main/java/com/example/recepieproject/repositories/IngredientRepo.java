package com.example.recepieproject.repositories;

import com.example.recepieproject.domain.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface IngredientRepo extends JpaRepository<Ingredient, Long> {
}
